---
title: Sauce aïoli
date: "2020-12-30T18:17:00.000Z"
description: "Sauce à l'ail traditionnelle"
---

## Ingrédients

1 jaune d'oeuf

4 gousses d'ail

20 cl d'huile d'olive

1 ou 2 cuillères à café de jus de citron (<= 1/2 citron>)

Sel

## Préparation

Ecraser l'ail

Mélanger le jaune d'oeuf, le sel et l'ail dans un petit bol

Ajouter quelques goutes d'huile et mélanger délicatement pendant 1 min avec un fouet

Répeter l'étape précédente pendant environ 10 min jusqu'à que la préparation soit épaisse

Quand 10 cl d'huile  sont versés, verser l'huile 2 cl par 2 cl, toujours en remuant pendant 1 min

Ajouter le jus de citron au gout