---
title: Îles flottantes de Michel Dumas
date: "2020-12-19T17:33:00.000Z"
description: "La recette classique des îles flottantes sur la crème anglaise nappées de caramel"
---
## Ingrédients

8 oeufs 

150g sucre cristal pour les jaunes

50g sucre cristal pour les blancs

Encore un peu de sucre cristal pour le caramel

rhum ou vanille

1L Lait

## Préparation

Séparer les blancs des jaunes

Réserver les blancs

### Crème anglaise

Ajouter les 150g sucre dans les jaunes

Ajouter un peu de rhum ou de vanille

Faire préchauffer le lait à 70C

Battre le mélange des jaunes

(Optionnel) Préparer un bain de glaçon pour refroidir à la fin

Quand le lait est à température, ajouter aux jaunes et mélanger

Remettre le mélange dans la casserole

Faire une cuisson progressive jusqu'à atteindre 80C en raclant bien pour obtenir une consistance nappante (Faire un trait sur la spatule avec le doigt pour vérifier) 

(Optionnel) Plonger la casserole dans le bain d'eau glacée

Laisser refroidir jusqu'à 50C

Réserver

### Blancs d'œuf

Faire chauffer une poêle avec un pouce d'eau

Faire préchauffer le four à 150C

Monter les blancs en neige très ferme 

Ajouter les 50g de sucre, battre à nouveau

Faire les îles grossièrement à la cuillère, les faire cuire 2min dans l'eau chaude

Faire cuire 3 min au four

Poser sur du papier absorbant

### Caramel

Mettre un peu de sucre dans une casserole

Ajouter un peu d'eau 

Faire tourner la casserole pour égaliser

Faire cuire à feu très doux sans mélanger

Attendre une belle couleur

Mettre dans l'eau froide pour arrêter la cuisson

### Dressage

Dans un bol ou une assiette creuse, mettre la crème anglaise, une île et napper de caramel.