---
title: Pâtes sauce Carbonara
date: "2021-01-05T18:48:00.000Z"
description: "La recette italienne sans crème"
---

## Ingrédients

500g de spaghetti / tagliatelles

250g de lardons

6 oeufs

Parmesan

Sel

Poivre

## Préparation

Faire cuire les pâtes <i>al dente</i> dans un grand volume d'eau salé

Pendant ce temps, casser les oeufs, mélanger avec le fromage, battre le tout, saler et poivrer

Faire cuire les lardons

Egouter les pâtes une fois cuites en gardant un peu d'eau

Ajouter les lardons avec leur jus de cuisson

Porter à feu très doux et ajouter la préparation d'oeufs en remuant sans arrêt

Servir chaud avec du fromage rapé
